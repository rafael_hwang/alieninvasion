#include "..\DiceInvaders.h"
#include "Ship.h"
#include "Settings.h"

using namespace std;

Ship::Ship(Settings* settings, IDiceInvaders* system)
	: m_is_drawn(false)
	, m_settings(settings)
	, m_right_left_is_move(false, false)
	, m_sz(32.0f, 32.0f)
	, m_pos(m_settings->get().at("ship_pos_x"), m_settings->get().at("ship_pos_y"))
	, m_x_offset(m_sz.first / 2.0f)
	, m_y_offset(m_sz.second / 2.0f)
	, m_x_start_end(m_pos.first - m_x_offset, m_pos.first + m_x_offset)
	, m_y_start_end(m_pos.second - m_y_offset, m_pos.second + m_y_offset)
	, m_system(system)
	, m_sprite(m_system->createSprite("data/player.bmp"))
{}
Ship::Ship(const Ship& o)
	: m_is_drawn(o.m_is_drawn)
	, m_settings(o.m_settings)
	, m_right_left_is_move(o.m_right_left_is_move)
	, m_sz(o.m_sz)
	, m_pos(o.m_pos)
	, m_x_offset(o.m_x_offset)
	, m_y_offset(o.m_y_offset)
	, m_x_start_end(o.m_x_start_end)
	, m_y_start_end(o.m_y_start_end)
	, m_system(o.m_system)
	, m_sprite(o.m_sprite)
{}
Ship::~Ship()
{
	if (m_is_drawn)
		m_sprite->destroy();
}

const pair<float, float>& Ship::get_pos() const
{
	return m_pos;
}

pair<float, float>& Ship::set_pos()
{
	return m_pos;
}

const pair<float, float>& Ship::get_sz() const
{
	return m_sz;
}

pair<float, float>& Ship::set_sz()
{
	return m_sz;
}

const pair<float, float>& Ship::get_x_range()
{
	m_x_start_end.first = m_pos.first - m_x_offset;
	m_x_start_end.second = m_pos.first + m_x_offset;
	return m_x_start_end;
}

const pair<float, float>& Ship::get_y_range()
{
	m_y_start_end.first = m_pos.second - m_y_offset;
	m_y_start_end.second = m_pos.second + m_y_offset;
	return m_y_start_end;
}

const pair<bool, bool>& Ship::get_right_left_is_move() const
{
	return m_right_left_is_move;
}

pair<bool, bool>& Ship::set_right_left_is_move()
{
	return m_right_left_is_move;
}

void Ship::draw()
{
	m_sprite->draw(m_pos.first, m_pos.second);
	m_is_drawn = true;
}

void Ship::update()
{
	if (m_right_left_is_move.first && m_pos.first < m_settings->get().at("screen_width") - m_sz.first)
	{
		m_pos.first += m_settings->get().at("ship_speed_factor");
		m_right_left_is_move.first = false;
	}
	if (m_right_left_is_move.second && m_pos.first > 0)
	{
		m_pos.first -= m_settings->get().at("ship_speed_factor");
		m_right_left_is_move.second = false;
	}
}

void Ship::reset()
{
	m_pos.first = m_settings->get().at("ship_pos_x");
	m_pos.second = m_settings->get().at("ship_pos_y");
}
