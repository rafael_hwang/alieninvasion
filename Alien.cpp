#include "..\DiceInvaders.h"
#include "Alien.h"
#include "Settings.h"
#include <cassert>

using namespace std;

Alien::Alien(Settings* settings, IDiceInvaders* system)
	: m_is_fired_drawn(false)
	, m_is_not_fired_drawn(false)
	, m_is_bomb_fired(false)
	, m_pos(0.0f, 0.0f)
	, m_sz(32.0f, 32.0f)
	, m_x_offset(m_sz.first / 2.0f)
	, m_y_offset(m_sz.second / 2.0f)
	, m_x_start_end(m_pos.first - m_x_offset, m_pos.first + m_x_offset)
	, m_y_start_end(m_pos.second - m_y_offset, m_pos.second + m_y_offset)
	, m_settings(settings)
	, m_system(system)	
	, m_sprite_fired(m_system->createSprite("data/enemy2-2.bmp"))
	, m_sprite_not_fired(m_system->createSprite("data/enemy2.bmp"))
{}
Alien::Alien(const Alien& o)
	: m_is_fired_drawn(o.m_is_fired_drawn)
	, m_is_not_fired_drawn(o.m_is_not_fired_drawn)
	, m_is_bomb_fired(o.m_is_bomb_fired)
	, m_pos(o.m_pos)
	, m_sz(o.m_sz)
	, m_x_offset(o.m_x_offset)
	, m_y_offset(o.m_y_offset)
	, m_x_start_end(o.m_x_start_end)
	, m_y_start_end(o.m_y_start_end)
	, m_settings(o.m_settings)
	, m_system(o.m_system)
	, m_sprite_fired(o.m_sprite_fired)
	, m_sprite_not_fired(o.m_sprite_not_fired)
{}
Alien::~Alien()
{
	if (m_is_fired_drawn)
		m_sprite_fired->destroy();
	if (m_is_not_fired_drawn)
		m_sprite_not_fired->destroy();
}
void Alien::draw()
{
	if (m_is_bomb_fired)
	{
		m_sprite_fired->draw(m_pos.first, m_pos.second);
		m_is_fired_drawn = true;
	}
	else
	{
		m_sprite_not_fired->draw(m_pos.first, m_pos.second);
		m_is_not_fired_drawn = true;
	}
}

void Alien::update()
{
	assert(m_settings->get().at("alien_direction") == 1.0f || m_settings->get().at("alien_direction") == -1.0f);
	m_pos.first += (m_settings->get().at("alien_speed_factor") * m_settings->get().at("alien_direction"));
}

const bool Alien::is_at_edge() const
{
	if ((m_pos.first >= m_settings->get().at("screen_width")-m_sz.first) || m_pos.first <= 0.0f)
		return true;
	return false;
}

const pair<float, float>& Alien::get_pos() const
{
	return m_pos;
}

pair<float, float>& Alien::set_pos()
{
	return m_pos;
}

const pair<float, float>& Alien::get_sz() const
{
	return m_sz;
}

pair<float, float>& Alien::set_sz()
{
	return m_sz;
}

const pair<float, float>& Alien::get_x_range()
{
	m_x_start_end.first = m_pos.first - m_x_offset;
	m_x_start_end.second = m_pos.first + m_x_offset;
	return m_x_start_end;
}

const pair<float, float>& Alien::get_y_range()
{
	m_y_start_end.first = m_pos.second - m_y_offset;
	m_y_start_end.second = m_pos.second + m_y_offset;
	return m_y_start_end;
}

const bool Alien::get_is_bomb_fired() const
{
	return m_is_bomb_fired;
}

void Alien::set_is_bomb_fired(bool b)
{
	m_is_bomb_fired = b;
}
