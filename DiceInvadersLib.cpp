#include <cassert>
#include "DiceInvadersLib.h"


DiceInvadersLib::DiceInvadersLib(const wchar_t* libraryPath)
{
	m_lib = LoadLibrary(libraryPath);
	assert(m_lib);

	DiceInvadersFactoryType* factory = (DiceInvadersFactoryType*)GetProcAddress(
		m_lib, "DiceInvadersFactory");
	m_interface = factory();
	assert(m_interface);
}

DiceInvadersLib::~DiceInvadersLib()
{
	FreeLibrary(m_lib);
}

IDiceInvaders* DiceInvadersLib::get() const
{
	return m_interface;
}
