#pragma once

#include <memory>

class Settings;
class IDiceInvaders;
class ISprite;

class Rocket
{
public:
	Rocket(Settings* settings, IDiceInvaders* system, const float ship_pos_x, const float ship_pos_y);
	Rocket(const Rocket& o);
	~Rocket();
	void draw();
	void update();
	const std::pair<float, float>& get_pos() const;
	const std::pair<float, float>& get_sz() const;
	const std::pair<float, float>& get_x_range();
	const std::pair<float, float>& get_y_range();

private:
	bool m_is_drawn;
	Settings* m_settings;
	float m_speed_factor;
	std::pair<float, float> m_sz;
	std::pair<float, float> m_pos;
	float m_x_offset;
	float m_y_offset;
	std::pair<float, float> m_x_start_end;
	std::pair<float, float> m_y_start_end;
	IDiceInvaders* m_system;
	ISprite* m_sprite;
};
