#pragma once

#include <vector>
#include <memory>

class Settings;
class IDiceInvaders;
class Ship;

class Scoreboard
{
public:
	Scoreboard(Settings* settings, IDiceInvaders* system);
	Scoreboard(const Scoreboard& o) = delete;
	Scoreboard(Scoreboard&& o) = delete;
	Scoreboard& operator=(const Scoreboard& o) = delete;
	Scoreboard& operator=(Scoreboard& o) = delete;
	~Scoreboard() = default;
	void draw();
	void setup_ships();

private:
	Settings* m_settings;
	IDiceInvaders* m_system;
	std::vector<std::unique_ptr<Ship>> m_lives;
	// void setup_level();
	// void setup_score();
	// void setup_high_score();
};
