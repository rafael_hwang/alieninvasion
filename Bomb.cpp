#include "..\DiceInvaders.h"
#include "Bomb.h"
#include "Settings.h"

using namespace std;


Bomb::Bomb(Settings* settings, IDiceInvaders* system, const float alien_pos_x, const float alien_pos_y)
	: m_is_drawn(false)
	, m_settings(settings)
	, m_speed_factor(m_settings->get().at("bomb_speed_factor"))
	, m_pos(alien_pos_x, alien_pos_y)
	, m_sz(32.0f, 32.0f)
	, m_x_offset(m_sz.first / 2.0f)
	, m_y_offset(m_sz.second / 2.0f)
	, m_x_start_end(m_pos.first - m_x_offset, m_pos.first + m_x_offset)
	, m_y_start_end(m_pos.second - m_y_offset, m_pos.second + m_y_offset)
	, m_system(system)
	, m_sprite(m_system->createSprite("data/bomb.bmp"))
{}

Bomb::Bomb(const Bomb& o)
	: m_is_drawn(o.m_is_drawn)
	, m_settings(o.m_settings)
	, m_speed_factor(o.m_speed_factor)
	, m_pos(o.m_pos)
	, m_sz(o.m_sz)
	, m_x_offset(o.m_x_offset)
	, m_y_offset(o.m_y_offset)
	, m_x_start_end(o.m_x_start_end)
	, m_y_start_end(o.m_y_start_end)
	, m_system(o.m_system)
	, m_sprite(o.m_sprite)
{}

Bomb::~Bomb()
{
	if (m_is_drawn)
		m_sprite->destroy();
}

void Bomb::draw()
{
	m_sprite->draw(m_pos.first, m_pos.second);
	m_is_drawn = true;
}

void Bomb::update()
{
	m_pos.second += m_speed_factor;
}

const pair<float, float>& Bomb::get_pos() const
{
	return m_pos;
}

pair<float, float>& Bomb::set_pos()
{
	return m_pos;
}

const pair<float, float>& Bomb::get_sz() const
{
	return m_sz;
}

pair<float, float>& Bomb::set_sz()
{
	return m_sz;
}

const pair<float, float>& Bomb::get_x_range()
{
	m_x_start_end.first = m_pos.first - m_x_offset;
	m_x_start_end.second = m_pos.first + m_x_offset;
	return m_x_start_end;
}

const pair<float, float>& Bomb::get_y_range()
{
	m_y_start_end.first = m_pos.second - m_y_offset;
	m_y_start_end.second = m_pos.second + m_y_offset;
	return m_y_start_end;
}
