#pragma once

#include <map>
#include <string>

class Settings
{
public:
    Settings();
    Settings(const Settings& o);
    ~Settings();
    const std::map<std::string, float>& get() const;
    std::map<std::string, float>& set();

private:
    std::map<std::string, float> m_m;
    void init_settings();
};
