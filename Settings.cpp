#include "Settings.h"

using namespace std;

Settings::Settings() { init_settings(); }
Settings::Settings(const Settings& o) : m_m(o.m_m) {}
Settings::~Settings() {};
const map<string, float>& Settings::get() const
{
    return m_m;
}
map<string, float>& Settings::set()
{
    return m_m;
}
void Settings::init_settings()
{
    m_m.emplace("game_active", 1.0);  // -1.0 inactive, 1.0 active
    m_m.emplace("screen_width", 640.0);
    m_m.emplace("screen_height", 480.0);
    m_m.emplace("ship_cnt", 3.0);
    m_m.emplace("ship_pos_x", 320.0);
    m_m.emplace("ship_pos_y", 480.0 - 32.0);
    m_m.emplace("ship_width", 32.0);
    m_m.emplace("ship_height", 32.0);
    m_m.emplace("ship_speed_factor", 1.0);
    m_m.emplace("rocket_speed_factor", 0.5);
    m_m.emplace("rocket_cnt", 1.0);
    m_m.emplace("alien_speed_factor", 0.1);
    m_m.emplace("alien_direction", 1.0);  // -1.0 left, 1.0 right
    m_m.emplace("alien_drop_speed", 6.15);
    m_m.emplace("bomb_speed_factor", 0.25);
    m_m.emplace("bomb_cnt", 1.0);
}
