#pragma once

#include <memory>

class IDiceInvaders;
class Settings;
class ISprite;

class Bomb
{
public:
	Bomb(Settings* settings, IDiceInvaders* system, const float alien_pos_x, const float alien_pos_y);
	Bomb(const Bomb& o);
	~Bomb();
	void draw();
	void update();
	const std::pair<float, float>& get_pos() const;
	std::pair<float, float>& set_pos();
	const std::pair<float, float>& get_sz() const;
	std::pair<float, float>& set_sz();
	const std::pair<float, float>& get_x_range();
	const std::pair<float, float>& get_y_range();

private:
	bool m_is_drawn;
	Settings* m_settings;
	float m_speed_factor;
	std::pair<float, float> m_pos;
	std::pair<float, float> m_sz;
	float m_x_offset;
	float m_y_offset;
	std::pair<float, float> m_x_start_end;
	std::pair<float, float> m_y_start_end;
	IDiceInvaders* m_system;
	ISprite* m_sprite;
};
