#pragma once

#include <memory>

class Settings;
class IDiceInvaders;
class ISprite;

class Ship
{
public:
	Ship(Settings* settings, IDiceInvaders* system);
	Ship(const Ship& o);
	~Ship();
	void draw();
	void update();
	void reset();
	const std::pair<float, float>& get_pos() const;
	std::pair<float, float>& set_pos();
	const std::pair<float, float>& get_sz() const;
	std::pair<float, float>& set_sz();
	const std::pair<bool, bool>& get_right_left_is_move() const;
	std::pair<bool, bool>& set_right_left_is_move();
	const std::pair<float, float>& get_x_range();
	const std::pair<float, float>& get_y_range();

private:
	bool m_is_drawn;
	Settings* m_settings;
	std::pair<bool, bool> m_right_left_is_move;
	std::pair<float, float> m_sz;
	std::pair<float, float> m_pos;
	float m_x_offset;
	float m_y_offset;
	std::pair<float, float> m_x_start_end;
	std::pair<float, float> m_y_start_end;
	IDiceInvaders* m_system;
	ISprite* m_sprite;	
};
