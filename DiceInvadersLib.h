#pragma once
#include <windows.h>
#include "..\DiceInvaders.h"

class DiceInvadersLib
{
public:
	explicit DiceInvadersLib(const wchar_t* libraryPath);
	DiceInvadersLib(const DiceInvadersLib&) = delete;
	DiceInvadersLib& operator=(const DiceInvadersLib&) = delete;
	~DiceInvadersLib();
	IDiceInvaders* get() const;

private:
	IDiceInvaders* m_interface;
	HMODULE m_lib;
};