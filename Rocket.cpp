#include "..\DiceInvaders.h"
#include "Rocket.h"
#include "Settings.h"

using namespace std;

Rocket::Rocket(Settings* settings, IDiceInvaders* system, const float ship_pos_x, const float ship_pos_y)
	: m_is_drawn(false)	
	, m_settings(settings)
	, m_sz(32.0f, 32.0f)
	, m_pos(ship_pos_x, ship_pos_y)
	, m_x_offset(m_sz.first / 2.0f)
	, m_y_offset(m_sz.second / 2.0f)
	, m_x_start_end(m_pos.first - m_x_offset, m_pos.first + m_x_offset)
	, m_y_start_end(m_pos.second - m_y_offset, m_pos.second + m_y_offset)
	, m_speed_factor(m_settings->get().at("rocket_speed_factor"))
	, m_system(system)
	, m_sprite(m_system->createSprite("data/rocket.bmp"))
{}
Rocket::Rocket(const Rocket& o)
	: m_is_drawn(o.m_is_drawn)
	, m_settings(o.m_settings)
	, m_sz(o.m_sz)
	, m_pos(o.m_pos)
	, m_x_offset(o.m_x_offset)
	, m_y_offset(o.m_y_offset)
	, m_x_start_end(o.m_x_start_end)
	, m_y_start_end(o.m_y_start_end)
	, m_speed_factor(o.m_speed_factor)
	, m_system(o.m_system)
	, m_sprite(o.m_sprite)
{}
Rocket::~Rocket()
{
	if (m_is_drawn)
		m_sprite->destroy();
}
void Rocket::draw()
{
	m_sprite->draw(m_pos.first, m_pos.second);
	m_is_drawn = true;
}
void Rocket::update()
{
	m_pos.second -= m_speed_factor;
}

const pair<float, float>& Rocket::get_pos() const
{
	return m_pos;
}

const pair<float, float>& Rocket::get_sz() const
{
	return m_sz;
}

const pair<float, float>& Rocket::get_x_range()
{
	m_x_start_end.first = m_pos.first - m_x_offset;
	m_x_start_end.second = m_pos.first + m_x_offset;
	return m_x_start_end;
}

const pair<float, float>& Rocket::get_y_range()
{
	m_y_start_end.first = m_pos.second - m_y_offset;
	m_y_start_end.second = m_pos.second + m_y_offset;
	return m_y_start_end;
}
