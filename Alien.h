#pragma once

#include <memory>

class Settings;
class IDiceInvaders;
class ISprite;

class Alien
{
public:
	Alien(Settings* settings, IDiceInvaders* system);
	Alien(const Alien& o);
	~Alien();
	void draw();
	void update();
	const bool is_at_edge() const;
	const std::pair<float, float>& get_pos() const;
	std::pair<float, float>& set_pos();
	const std::pair<float, float>& get_sz() const;
	std::pair<float, float>& set_sz();
	const bool get_is_bomb_fired() const;
	void set_is_bomb_fired(bool b);
	const std::pair<float, float>& get_x_range();
	const std::pair<float, float>& get_y_range();

private:
	bool m_is_fired_drawn;
	bool m_is_not_fired_drawn;
	bool m_is_bomb_fired;
	std::pair<float, float> m_pos;
	std::pair<float, float> m_sz;
	float m_x_offset;
	float m_y_offset;
	std::pair<float, float> m_x_start_end;
	std::pair<float, float> m_y_start_end;
	Settings* m_settings;
	IDiceInvaders* m_system;
	ISprite* m_sprite_not_fired;
	ISprite* m_sprite_fired;
};
