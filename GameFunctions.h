#pragma once

#include <vector>
#include <memory>

class Settings;
class IDiceInvaders;
class Scoreboard;
class ISprite;
class Ship;
class Alien;
class Rocket;
class Bomb;

class GameFunctions
{
	using UPtrShip = std::unique_ptr<Ship>;
	using UPtrAlien = std::unique_ptr<Alien>;
	using UPtrBomb = std::unique_ptr<Bomb>;
	using UPtrRocket = std::unique_ptr<Rocket>;
	using UPtrScoreboard = std::unique_ptr<Scoreboard>;

public:
	GameFunctions() = default;
	GameFunctions(const GameFunctions& o) = delete;
	GameFunctions(GameFunctions&& o) = delete;
	GameFunctions& operator=(const GameFunctions& o) = delete;
	GameFunctions& operator=(GameFunctions& o) = delete;
	~GameFunctions() = default;

	void create_aliens(
		Settings* settings,
		IDiceInvaders* system,
		std::vector<UPtrAlien>& aliens);

	void check_events(
		Settings* settings,
		IDiceInvaders* system,
		const UPtrShip& ship,
		std::vector<UPtrAlien>& aliens,
		std::vector<UPtrRocket>& rockets,
		std::vector<std::pair<UPtrBomb,
		int>>& bombs);

	void update_rockets(
		Settings* settings,
		const UPtrShip& ship,
		std::vector<UPtrAlien>& aliens,
		std::vector<UPtrRocket>& rockets,
		IDiceInvaders* system);

	void update_bombs(
		Settings* settings,
		const UPtrShip& ship,
		std::vector<UPtrRocket>& rockets,
		std::vector<UPtrAlien>& aliens,
		std::vector<std::pair<UPtrBomb, int>>& bombs,
		IDiceInvaders* system,
		UPtrScoreboard& scoreboard);

	void update_screen(
		std::vector<UPtrAlien>& aliens,
		std::vector<UPtrRocket>& rockets,
		const UPtrShip& ship,
		std::vector<std::pair<UPtrBomb,int>>& bombs,
		UPtrScoreboard& scoreboard);

	void change_aliens_direction(
		Settings* settings,
		std::vector<UPtrAlien>& aliens);

	void check_aliens_edges(
		Settings* settings,
		std::vector<UPtrAlien>& aliens);

	void check_aliens_bottom(
		Settings* settings,
		IDiceInvaders* system,
		std::vector<UPtrAlien>& aliens,
		std::vector<UPtrRocket>& rockets,
		const UPtrShip& ship,
		std::vector<std::pair<UPtrBomb, int>>& bombs,
		UPtrScoreboard& scoreboard);

	void update_when_ship_hit(
		Settings* settings, IDiceInvaders* system,
		const UPtrShip& ship,
		std::vector<UPtrAlien>& aliens,
		std::vector<UPtrRocket>& rockets,
		std::vector<std::pair<UPtrBomb, int>>& bombs,
		UPtrScoreboard& scoreboard);

	void update_aliens(
		Settings* settings,
		IDiceInvaders* system,
		const UPtrShip& ship,
		std::vector<UPtrAlien>& aliens,
		std::vector<UPtrRocket>& rockets,
		std::vector<std::pair<UPtrBomb, int>>& bombs,
		UPtrScoreboard& scoreboard);

private:
	void create_alien(
		Settings* settings,
		IDiceInvaders* system,
		std::vector<UPtrAlien>& aliens,
		int row_num,
		int alien_num,
		const std::pair<int, int>& alien_sz);

	int get_n_aliens_across(
		Settings* settings,
		int alien_width);

	int get_n_rows(
		Settings* settings,
		int alien_height);

	void create_rocket(
		Settings* settings,
		const UPtrShip& ship,
		std::vector<UPtrRocket>& rockets,
		IDiceInvaders* system);

	void create_bomb(
		Settings* settings,
		std::vector<UPtrAlien>& aliens,
		std::vector<std::pair<UPtrBomb, int>>& bombs,
		IDiceInvaders* system);

	int get_collided_alien_idx(
		const UPtrShip& ship,
		std::vector<UPtrAlien>& aliens) const;

	bool is_bomb_ship_collision(
		std::vector<std::pair<UPtrBomb, int>>& bombs,
		const UPtrShip& ship);

	void check_rocket_alien_collison(
		Settings* settings,
		std::vector<UPtrAlien>& aliens,
		std::vector<UPtrRocket>& rockets,
		IDiceInvaders* system);
};
