#include "..\DiceInvaders.h"
#include "GameFunctions.h"
#include "Scoreboard.h"
#include "Alien.h"
#include "Ship.h"
#include "Rocket.h"
#include "Settings.h"
#include "Bomb.h"

using namespace std;


int GameFunctions::get_n_aliens_across(Settings* settings, int alien_width)
{
	auto x_space = static_cast<int>(settings->get().at("screen_width") - 2 * alien_width);
	auto n_aliens_across = static_cast<int>(x_space / (2 * alien_width));
	return n_aliens_across;
}

int GameFunctions::get_n_rows(Settings* settings, int alien_height)
{
	auto y_space = static_cast<int>(
		static_cast<int>(settings->get().at("screen_height"))
		- (7 * alien_height)
		- static_cast<int>(settings->get().at("ship_height"))
	);
	auto n_rows = static_cast<int>(y_space / (2 * alien_height));
	return n_rows;
}

void GameFunctions::create_alien(
	Settings* settings,
	IDiceInvaders* system,
	vector<UPtrAlien>& aliens,
	int row_num,
	int alien_num,
	const pair<int, int>& alien_sz)
{
	auto alien = make_unique<Alien>(settings, system);
	alien->set_pos().first = alien_sz.first + (2.0 * alien_sz.first * alien_num);
	alien->set_pos().second = alien_sz.second + (2.0 * alien_sz.second * row_num);
	aliens.push_back(move(alien));
}

void GameFunctions::create_aliens(Settings* settings, IDiceInvaders* system, vector<UPtrAlien>& aliens)
{
	auto alien = Alien(settings, system);
	auto sz = alien.get_sz();
	int n_aliens_across = get_n_aliens_across(settings, alien.get_sz().first);
	int n_rows = get_n_rows(settings, alien.get_sz().second);
	for (auto r = 0; r < n_rows; ++r)
		for (auto a = 0; a < n_aliens_across; ++a)
			create_alien(settings, system, aliens, r, a, sz);
}

void GameFunctions::check_events(
	Settings* settings,
	IDiceInvaders* system,
	const UPtrShip& ship,
	vector<UPtrAlien>& aliens,
	vector<UPtrRocket>& rockets,
	vector<pair<UPtrBomb, int>>& bombs)
{
	IDiceInvaders::KeyStatus keys;
	system->getKeyStatus(keys);
	if (keys.right)
		ship->set_right_left_is_move().first = true;
	else if (keys.left)
		ship->set_right_left_is_move().second = true;
	else if (keys.fire)
		create_rocket(settings, ship, rockets, system);
	create_bomb(settings, aliens, bombs, system);
}

void GameFunctions::create_rocket(
	Settings* settings,
	const UPtrShip& ship,
	vector<UPtrRocket>& rockets,
	IDiceInvaders* system)
{
	if (static_cast<int>(rockets.size()) < settings->get().at("rocket_cnt"))
	{
		auto rocket = make_unique<Rocket>(settings, system, ship->get_pos().first, ship->get_pos().second);
		rockets.push_back(move(rocket));
	}
}

void GameFunctions::update_rockets(
	Settings* settings,
	const UPtrShip& ship,
	vector<UPtrAlien>& aliens,
	vector<UPtrRocket>& rockets,
	IDiceInvaders* system)
{
	for (const auto& r : rockets)
		r->update();
	vector<unsigned> to_delete;
	for (auto i = 0; i < static_cast<int>(rockets.size()); ++i)
		if (rockets.at(i)->get_pos().second < 0)
			to_delete.push_back(i);
	for (auto i = 0; i < static_cast<int>(to_delete.size()); ++i)
		rockets.erase(rockets.begin() + i);
	check_rocket_alien_collison(settings, aliens, rockets, system);
}

void GameFunctions::create_bomb(
	Settings* settings,
	vector<UPtrAlien>& aliens,
	vector<pair<UPtrBomb, int>>& bombs,
	IDiceInvaders* system)
{
	if (static_cast<float>(bombs.size()) < settings->get().at("bomb_cnt"))
	{
		auto sz = static_cast<int>(aliens.size());
		auto j = rand() % sz;
		for (auto i = 0; i < sz; ++i)
		{
			if (i == j)
			{
				auto bomb = make_unique<Bomb>(settings, system, aliens.at(i)->get_pos().first, aliens.at(i)->get_pos().second);				
				if (!aliens.at(i)->get_is_bomb_fired())
					aliens.at(i)->set_is_bomb_fired(true);
				bombs.emplace_back(move(bomb), i);
			}
		}
	}
}

void GameFunctions::update_bombs(
	Settings* settings,
	const UPtrShip& ship,
	vector<UPtrRocket>& rockets,
	vector<UPtrAlien>& aliens,
	vector<pair<UPtrBomb, int>>& bombs,
	IDiceInvaders* system,
	UPtrScoreboard& scoreboard)
{	
	for (const auto& b : bombs)
		b.first->update();
	vector<unsigned> to_delete;
	for (auto i = 0; i < static_cast<int>(bombs.size()); ++i)
		if (bombs.at(i).first->get_pos().second >= settings->get().at("screen_height"))
		{
			to_delete.push_back(i);
			auto alien_idx = bombs.at(i).second;
			// make sure the alien still exists..
			if (static_cast<int>(aliens.size()) > alien_idx)
				aliens.at(alien_idx)->set_is_bomb_fired(false);
		}
	for (auto i = 0; i < static_cast<int>(to_delete.size()); ++i)
		bombs.erase(bombs.begin() + i);
	if (is_bomb_ship_collision(bombs, ship))
		update_when_ship_hit(settings, system, ship, aliens, rockets, bombs, scoreboard);
}

void GameFunctions::update_screen(
	vector<UPtrAlien>& aliens,
	vector<UPtrRocket>& rockets,
	const UPtrShip& ship,
	vector<pair<UPtrBomb, int>>& bombs,
	unique_ptr<Scoreboard>& scoreboard)
{
	ship->draw();
	for (const auto& a : aliens)
		a->draw();
	for (const auto& r : rockets)
		r->draw();
	for (const auto& b : bombs)
		b.first->draw();
	scoreboard->draw();
}

void GameFunctions::change_aliens_direction(Settings* settings, vector<UPtrAlien>& aliens)
{
	for (const auto& a : aliens)
		a->set_pos().second += settings->get().at("alien_drop_speed");
	settings->set().at("alien_direction") *= -1.0;
}

void GameFunctions::check_aliens_edges(Settings* settings, vector<UPtrAlien>& aliens)
{
	for (const auto& a : aliens)
	{
		if (a->is_at_edge())
		{
			change_aliens_direction(settings, aliens);
			break;
		}
	}
}

void GameFunctions::update_when_ship_hit(
	Settings* settings,
	IDiceInvaders* system,
	const UPtrShip& ship,
	vector<UPtrAlien>& aliens,
	vector<UPtrRocket>& rockets,
	vector<pair<UPtrBomb, int>>& bombs,
	UPtrScoreboard& scoreboard)
{
	if (settings->get().at("ship_cnt") > 0)
	{
		settings->set().at("ship_cnt") -= 1;
		scoreboard->setup_ships();
		aliens.clear();
		rockets.clear();
		bombs.clear();
		create_aliens(settings, system, aliens);
		ship->reset();
		// TODO sleep 0.5
	}
	else
	{
		settings->set().at("game_active") = -1.0;
	}
}

void GameFunctions::check_aliens_bottom(
	Settings* settings,
	IDiceInvaders* system,
	vector<UPtrAlien>& aliens,
	vector<UPtrRocket>& rockets,
	const UPtrShip& ship,
	vector<pair<UPtrBomb, int>>& bombs,
	UPtrScoreboard& scoreboard)
{
	for (const auto& a : aliens)
	{
		if (a->get_pos().second >= settings->get().at("screen_height")-a->get_sz().second)
		{
			update_when_ship_hit(settings, system, ship, aliens, rockets, bombs, scoreboard);
			break;
		}
	}
}

void GameFunctions::update_aliens(
	Settings* settings,
	IDiceInvaders* system,
	const UPtrShip& ship,
	vector<UPtrAlien>& aliens,
	vector<UPtrRocket>& rockets,
	vector<pair<UPtrBomb, int>>& bombs,
	UPtrScoreboard& scoreboard)
{
	check_aliens_edges(settings, aliens);
	for (const auto& a : aliens)
		a->update();
	if (get_collided_alien_idx(ship, aliens) > -1)
		update_when_ship_hit(settings, system, ship, aliens, rockets, bombs, scoreboard);
	check_aliens_bottom(settings, system, aliens, rockets, ship, bombs, scoreboard);
}

int GameFunctions::get_collided_alien_idx(const UPtrShip& ship, vector<UPtrAlien>& aliens) const
{
	auto idx = -1;
	auto ship_x_start = ship->get_x_range().first;
	auto ship_x_end = ship->get_x_range().second;
	auto ship_y_start = ship->get_y_range().first;
	auto ship_y_end = ship->get_y_range().second;

	for (auto i = 0; i < static_cast<int>(aliens.size()); ++i)
	{
		auto alien_x_start = aliens.at(i)->get_x_range().first;
		auto alien_x_end = aliens.at(i)->get_x_range().second;
		auto alien_y_start = aliens.at(i)->get_y_range().first;
		auto alien_y_end = aliens.at(i)->get_y_range().second;

		if (((ship_x_start <= alien_x_start && alien_x_start <= ship_x_end)
				&& (ship_y_start <= alien_y_start && alien_y_start <= ship_y_end))
			|| ((ship_x_start <= alien_x_end && alien_x_end <= ship_x_end)
				&& (ship_y_start <= alien_y_end && alien_y_end <= ship_y_end)))
		{
			idx = i;
			break;
		}
	}

	return idx;
}

bool GameFunctions::is_bomb_ship_collision(vector<pair<UPtrBomb, int>>& bombs, const UPtrShip& ship)
{
	auto ret = false;
	auto ship_x_start = ship->get_x_range().first;
	auto ship_x_end = ship->get_x_range().second;
	auto ship_y_start = ship->get_y_range().first;
	auto ship_y_end = ship->get_y_range().second;

	for (auto i = 0; i < static_cast<int>(bombs.size()); ++i)
	{
		auto bomb_x_start = bombs.at(i).first->get_x_range().first;
		auto bomb_x_end = bombs.at(i).first->get_x_range().second;
		auto bomb_y_start = bombs.at(i).first->get_y_range().first;
		auto bomb_y_end = bombs.at(i).first->get_y_range().second;

		if (((ship_x_start <= bomb_x_start && bomb_x_start <= ship_x_end)
				&& (ship_y_start <= bomb_y_start && bomb_y_start <= ship_y_end))
			|| ((ship_x_start <= bomb_x_end && bomb_x_end <= ship_x_end)
				&& (ship_y_start <= bomb_y_end && bomb_y_end <= ship_y_end)))
		{
			ret = true;
			break;
		}
	}
	return ret;
}

void GameFunctions::check_rocket_alien_collison(
	Settings* settings,
	vector<UPtrAlien>& aliens,
	vector<UPtrRocket>& rockets,
	IDiceInvaders* system)
{
	vector<int> aliens_idx_to_remove;

	for (const auto& r : rockets)
	{
		auto rocket_x_start = r->get_x_range().first;
		auto rocket_x_end = r->get_x_range().second;
		auto rocket_y_start = r->get_y_range().first;
		auto rocket_y_end = r->get_y_range().second;

		for (auto i = 0; i < static_cast<int>(aliens.size()); ++i)
		{
			auto alien_x_start = aliens.at(i)->get_x_range().first;
			auto alien_x_end = aliens.at(i)->get_x_range().second;
			auto alien_y_start = aliens.at(i)->get_y_range().first;
			auto alien_y_end = aliens.at(i)->get_y_range().second;

			if (((rocket_x_start <= alien_x_start && alien_x_start <= rocket_x_end)
					&& (rocket_y_start <= alien_y_start && alien_y_start <= rocket_y_end))
				|| ((rocket_x_start <= alien_x_end && alien_x_end <= rocket_x_end)
					&& (rocket_y_start <= alien_y_end && alien_y_end <= rocket_y_end)))
			{
				aliens_idx_to_remove.push_back(i);
			}
		}
	}

	for (const auto& idx : aliens_idx_to_remove)
	{
		// TODO swap alien to "hit" sprite
		aliens.erase(aliens.begin() + idx);
		if (aliens.size() == 0)
		{
			rockets.empty();
			// TODO: new level -increase speeds
			create_aliens(settings, system, aliens);
		}
	}
}
