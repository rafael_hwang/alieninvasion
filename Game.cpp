#include "DiceInvadersLib.h"
#include "GameFunctions.h"
#include "Scoreboard.h"
#include "Settings.h"
#include "Ship.h"
#include "Alien.h"
#include "Rocket.h"
#include "Bomb.h"

using namespace std;


int APIENTRY WinMain(HINSTANCE instance, HINSTANCE previousInstance,
	LPSTR commandLine, int commandShow)
{
	DiceInvadersLib lib(L"DiceInvaders.dll");
	IDiceInvaders* system = lib.get();
	vector<unique_ptr<Alien>> aliens;
	vector<unique_ptr<Rocket>> rockets;
	vector<pair<unique_ptr<Bomb>, int>> bombs;
	auto gf = make_unique<GameFunctions>();
	Settings set = Settings();
	Settings* settings = &set;
	system->init(static_cast<int>(settings->get().at("screen_width")),
		static_cast<int>(settings->get().at("screen_height")));
	auto scoreboard = make_unique<Scoreboard>(settings, system);
	auto ship = make_unique<Ship>(settings, system);
	gf->create_aliens(settings, system, aliens);
		
	while (settings->get().at("game_active") != -1.0)
	{
		gf->check_events(settings, system, ship, aliens, rockets, bombs);
		if (system->update())
		{
			ship->update();
			gf->update_rockets(settings, ship, aliens, rockets, system);
			gf->update_aliens(settings, system, ship, aliens, rockets, bombs, scoreboard);
			gf->update_bombs(settings, ship, rockets, aliens, bombs, system, scoreboard);
		}
		gf->update_screen(aliens, rockets, ship, bombs, scoreboard);
	}
	
	system->destroy();
	return 0;
}
