#include "..\DiceInvaders.h"
#include"Settings.h"
#include "Ship.h"
#include "Scoreboard.h"

using namespace std;


Scoreboard::Scoreboard(Settings* settings, IDiceInvaders* system)
	: m_lives()
	, m_settings(settings)
	, m_system(system)
{
	setup_ships();
}

void Scoreboard::draw()
{
	for (const auto& ship : m_lives)
		ship->draw();
}

void Scoreboard::setup_ships()
{
	if (!m_lives.empty())
		m_lives.clear();
	for (auto i = 0; i < static_cast<int>(m_settings->get().at("ship_cnt")); ++i)
	{
		auto ship = make_unique<Ship>(m_settings, m_system);
		ship->set_pos().first = 10 + i * ship->get_sz().second;
		ship->set_pos().second = 10;
		m_lives.push_back(move(ship));
	}
}
